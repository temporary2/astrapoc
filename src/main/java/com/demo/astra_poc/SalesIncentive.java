package com.demo.astra_poc;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class SalesIncentive implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@org.kie.api.definition.type.Label("ID Sales")
	private java.lang.Integer idSales;
	@org.kie.api.definition.type.Label("Scheme")
	private java.lang.String scheme;
	@org.kie.api.definition.type.Label("Incentive")
	private java.lang.Double incentive;

	@org.kie.api.definition.type.Label("Quantity")
	private java.lang.Integer qty;

	@org.kie.api.definition.type.Label("Total Basic Incentive")
	private java.lang.Integer basicIncentive;

	@org.kie.api.definition.type.Label(value = "Sales Category")
	private java.lang.String salesCategory;

	@org.kie.api.definition.type.Label(value = "Bonus Achieve Target")
	private java.lang.Integer bonusAchieveTarget;

	public SalesIncentive() {
	}

	public java.lang.Integer getIdSales() {
		return this.idSales;
	}

	public void setIdSales(java.lang.Integer idSales) {
		this.idSales = idSales;
	}

	public java.lang.String getScheme() {
		return this.scheme;
	}

	public void setScheme(java.lang.String scheme) {
		this.scheme = scheme;
	}

	public java.lang.Double getIncentive() {
		return this.incentive;
	}

	public void setIncentive(java.lang.Double incentive) {
		this.incentive = incentive;
	}

	public java.lang.Integer getQty() {
		return this.qty;
	}

	public void setQty(java.lang.Integer qty) {
		this.qty = qty;
	}

	public java.lang.Integer getBasicIncentive() {
		return this.basicIncentive;
	}

	public void setBasicIncentive(java.lang.Integer basicIncentive) {
		this.basicIncentive = basicIncentive;
	}

	public java.lang.String getSalesCategory() {
		return this.salesCategory;
	}

	public void setSalesCategory(java.lang.String salesCategory) {
		this.salesCategory = salesCategory;
	}

	public java.lang.Integer getBonusAchieveTarget() {
		return this.bonusAchieveTarget;
	}

	public void setBonusAchieveTarget(java.lang.Integer bonusAchieveTarget) {
		this.bonusAchieveTarget = bonusAchieveTarget;
	}

	public SalesIncentive(java.lang.Integer idSales, java.lang.String scheme,
			java.lang.Double incentive, java.lang.Integer qty,
			java.lang.Integer basicIncentive, java.lang.String salesCategory,
			java.lang.Integer bonusAchieveTarget) {
		this.idSales = idSales;
		this.scheme = scheme;
		this.incentive = incentive;
		this.qty = qty;
		this.basicIncentive = basicIncentive;
		this.salesCategory = salesCategory;
		this.bonusAchieveTarget = bonusAchieveTarget;
	}

}